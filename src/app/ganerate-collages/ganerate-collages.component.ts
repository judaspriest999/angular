import { Component, OnInit } from '@angular/core';

import {Source} from '../source-list/source';

@Component({
  selector: 'ganerate-collages',
  templateUrl: './ganerate-collages.component.html',
  styleUrls: ['./ganerate-collages.component.scss']
})
export class GanerateCollagesComponent implements OnInit {

  private sources: string[];

  constructor(private source:Source) {
    this.sources = source.sources;
   }

  ngOnInit() {
  }

}
