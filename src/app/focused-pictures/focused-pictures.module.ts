import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule, MatToolbarModule, MatCardModule, MatIconModule } from '@angular/material';
import { PipFocusedModule } from 'pip-webui2-behaviors';
import { PipPictureModule} from 'pip-webui2-pictures';

import{FocusedPicturesComponent} from './focused-pictures.component';

@NgModule({
  declarations: [FocusedPicturesComponent],
  imports: [
    CommonModule,

    FlexLayoutModule,
    MatListModule,
    MatToolbarModule, 
    MatCardModule,
    MatIconModule,

    PipPictureModule,

    PipFocusedModule,
  ],
})
export class FocusedPicturesModule { }
