import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AppTranslations } from './app.strings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public constructor(
    private translate: TranslateService,
  ) {
    this.translate.use('en');
    this.translate.setTranslation('en', AppTranslations.en, true);
    this.translate.setTranslation('ru', AppTranslations.ru, true);
  }

}
