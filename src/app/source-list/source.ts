import { Injectable } from '@angular/core';

@Injectable()
export class Source{
    sources: string[] = [
        './assets/boy.png',
        './assets/girl2.png',
        './assets/boy2.png',
        './assets/girl.png',
        './assets/pixel.png',
        './assets/pixel2.png',
        './assets/boy3.png',
        './assets/ninja.png',
        //'./assets/boy5.jpg',
        //'./assets/boy6.png',
        './assets/boy4.png',        
    ];
}
